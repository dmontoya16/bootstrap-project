$(function () {
  $("[data-toggle='tooltip']").tooltip();
  $("[data-toggle='popover']").popover();
  $('.carousel').carousel({
    interval: 3000,
  });
  $('#contactModal').on('show.bs.modal', function (e){
    $('#contactBtn').removeClass('btn-outline-danger');
    $('#contactBtn').addClass('btn-outline-primary');
    $('#contactBtn').prop('disabled',true);
  });
  $('#contactModal').on('hidden.bs.modal', function (e){
    $('#contactBtn').prop('disabled',false);
  });
});

$('.top-navbar').load('navbar.html');
$('.top-header').load('header.html');
$('.top-footer').load('footer.html');
